![LabelREM](asset/labelrem_logo.png "LabelREM")

LabelREM is a lightweight desktop labeling tool that is written with Python and using mainly OpenCV

![LabelREM SS](asset/labelrem_ss.png "LabelREM Screenshot")

## Usage

python labelrem.py <dataset_directory>

Draw a rectangle on desired region, write the content and hit 'Enter' to save annotation. Field names are filled based upon `template.json` file in `dataset` directory. If all fields are done, drawing is disabled and go to next image.

Press 'Esc' or 'q' to exit.

## Installation

### GNU/Linux

Requirements are given in `requirements.txt`.

`pip install -r requirements.txt`

### Windows

In Git Bash console crete an environment with given `requirements.txt` and make an executable file with

`bash build_windows.sh`

This will create `dist/labelrem.exe` . Use this executable file with a `dataset` directory as given in the repository.

```
labelrem.exe
dataset
|   price-tag
|   |   images/
|   |   template.json
```

## Shortcuts
* `Esc` : Exit
* `q` : Exit
* `a` : Previous image
* `d` : Next image
* `z` : Undo the last annotation
* `c` : Clean all annotations
